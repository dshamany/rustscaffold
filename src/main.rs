use actix_web::{web, App, HttpServer};
use actix_files as fs;
use dotenv::dotenv;

mod config;
mod handlers;
mod routes;

use crate::config::handlebars::handlebars_config;
use crate::routes::scopes;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    // PORT
    let port = std::env::var("PORT").unwrap_or("8080".to_string());
    println!("Starting Actix Web on Port: {}", &port);

    // HANDLEBARS
    let hdlbrs = web::Data::new(handlebars_config());

    HttpServer::new(move || {
        App::new()
            .app_data(hdlbrs.clone())
            .service(fs::Files::new("/files/styles", "static/styles"))
            .service(fs::Files::new("/files/images", "static/images"))
            .service(scopes::healthcheck())
            .service(scopes::views())
            .service(scopes::storage_engine())
    })
    .bind(format!("0.0.0.0:{}", port))?
    .run()
    .await
}
