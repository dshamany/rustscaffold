use actix_multipart::{Multipart, MultipartError};
use actix_web::http::StatusCode;
use actix_web::web::Json;
use actix_web::{post, Error, HttpResponse, HttpResponseBuilder};
use digital_legacy_core::core::key_manager::{hash_strings, SALT_AUTO};
use digital_legacy_core::core::storage::{
    get_file_from_storage_with_passphrase, save_file_to_storage,
};
use digital_legacy_core::integrations::mailer;
use futures::StreamExt;
use serde::{Deserialize, Serialize};
use std::io::Write;
use zip::write::FileOptions;
use zip::ZipWriter;
use std::io::Cursor;

async fn handle_multipart_payload(
    mut payload: Multipart,
) -> Result<(String, Vec<String>), MultipartError> {
    // Create Results
    let mut recipient = String::new();
    let mut files = vec![];

    // Parse multipart form
    while let Some(item) = payload.next().await {
        let mut field = item?;

        let content_disposition = field.content_disposition();
        let field_name = content_disposition.get_name().map(|n| n.to_string());

        if let Some(name) = field_name {
            if name == "recipient" {
                let mut value = vec![];
                while let Some(chunk) = field.next().await {
                    let data = chunk?;
                    value.extend_from_slice(&data);
                }
                recipient = String::from_utf8(value).unwrap();
            } else {
                let filename = content_disposition
                    .get_filename()
                    .map_or_else(|| "".to_string(), |v| v.to_string());

                if !filename.is_empty() {
                    let filepath = format!("/tmp/{}", sanitize_filename::sanitize(&filename));

                    let mut file = std::fs::File::create(&filepath).unwrap();
                    while let Some(chunk) = field.next().await {
                        let data = chunk.unwrap();
                        file.write_all(&data).unwrap();
                    }

                    files.push(filepath);
                }
            }
        }
    }
    return Ok((recipient, files));
}

#[post("/upload")]
pub async fn upload(payload: Multipart) -> Result<HttpResponse, Error> {
    let payload = handle_multipart_payload(payload).await?;
    let files: Vec<&str> = payload.1.iter().map(|s| s.as_str()).collect();

    let res = save_file_to_storage("test", None, &files, None).await?;

    let recipient = payload.0;
    let from = std::env::var("GOOGLE_USERNAME").unwrap();
    let subject = "File uploaded";
    let body = res.0;
    let _ = mailer::send_message(&from, &recipient, &subject, &body);

    Ok(HttpResponse::Created().body(format!("Files uploaded: {:?}", files.len())))
}

#[derive(Debug, Deserialize, Serialize)]
pub struct DownloadResponse {
    pub passphrase: String,
}
