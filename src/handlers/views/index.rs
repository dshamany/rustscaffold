use actix_web::{get, web, HttpResponse, Responder};
use serde_json::json;

use handlebars::Handlebars;

#[get("/home")]
async fn home(hb: web::Data<Handlebars<'_>>) -> impl Responder {
    let data = json!({
        "name": "from Actix Web, using Handlebars and HTMX",
    });
    let view = hb.render("home", &data).unwrap();
    HttpResponse::Ok().body(view)
}
