use handlebars::Handlebars;

pub fn handlebars_config() -> Handlebars<'static> {
    let mut handlebars = Handlebars::new();
    handlebars
        .register_templates_directory(".html", "./static/templates")
        .unwrap();
    handlebars
}
