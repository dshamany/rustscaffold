use actix_web::{web, Scope};

use crate::handlers::{api, views};

pub fn healthcheck() -> Scope {
    web::scope("/healthcheck").service(api::health::check)
}

pub fn views() -> Scope {
    web::scope("/views").service(views::index::home)
}

pub fn storage_engine() -> Scope {
    web::scope("/storage")
        .service(api::storage_engine::upload)
}
