# Rust Scaffold

## Table of Contents
1. [Actix Web](#ActixWeb)
2. [Handlebars](#Handlebars)
3. [HTMX](#HTMX)
4. [Tailwind CSS](#TailwindCSS)
5. [Why Server-Side-Rendering](#SSR)
6. [Usage](#Usage)
7. [Caveats](#Caveats)

## Actix Web<a name="ActixWeb"></a>
The backend framework used here is Actix Web because it seemed easier to use compared to Rocket (which I used before). Additionally, it is better documented and feels faster (at least the last time I tried it).
## Handlebars<a name="Handlebars"></a>
The reason for Handlebars is familiarity with the framework and it allows the reuse of partials in a way I like, so I descided to include it in order to facilitate efficient template generation.

## HTMX<a name="HTMX"></a>
The use of HTMX is the result of my attempt to eliminate front end business logic, and based on my investigation, it appears to do the job perfectly. I'm no HTMX expert; nonetheless, it can easily be removed by removing the CDN import in the script tag inside `src/static/templates/index.html`, which is used as a basis for all CDN imports and a header for all other templates.

## Tailwind CSS<a name="TailwindCSS"></a>
The reason I chose to import tailwind CSS is because it looks more modern than bootstrap and allows for ample customizability. One way I would use tailwind is by creating components using templates and later import them onto partial views that are then returned by Actix Web for a smooth experience.

Should you wish to remove tailwind CSS for whatever reason, you only need to remove the CDN import from `src/static/templates/index.html`.

## Why a Server-Side-Rendered App<a name="SSR"></a>
Server side rendering allows the programmer to keep the entire project in one repository; moreover, with Rust, a compiled binary is the surest way to keep all the code consistent across machines and inside containers.

Another benefit with this particular scaffold is the use of HTMX (reasoning above), which allows the reuse of templates whereby Actix Web returns pieces of HTML instead of JSON. This alleviates the need for front end business logic to parse the JSON and render the page. If we wish to handle errors, we may instead return an HTML snippet that displays "User not found" instead of managing errors on the front end. The application should be snappier as a consequence.

## Usage<a name="Usage"></a>

#### Clone the repository
```
  git clone git@gitlab.com:dshamany/rustscaffold.git <name-of-app>
```

#### Change Directory
```
  cd rustscaffold
```

#### Cargo Run
```
  cargo run
```

Use `http://localhost:8080/views/home` to access the site

#### Container Run
```
  cd path/to/Dockerfile
  docker build -t <name-of-app> .
  docker run --rm -p <port-of-choice>:8080 <name-of-app>:latest
```

Use `http://localhost:<port-of-choice>/views/home` to access the site

## Caveats<a name="Caveats"></a>

#### Cargo.toml
The default name for the scaffold is "app", which produces an executable called "app" inside the OCI container in the Dockerfile manifest.

#### Server Bind Address
The server bind address is "0.0.0.0" and the port can be set using environment variables. The reason for the loopback address is so that the application is able to receive incoming requests from outside the containerized application. You will still be able to run `cargo run` and test your app locally.

**Default Port is 8080** but can be changed using an environment variable as such `PORT=<PORT-NUMBER>`
**PS!** Please ensure you change line 6 to expose the correct port when using the app from the OCI container.
