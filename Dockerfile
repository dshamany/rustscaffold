FROM rust:latest AS builder
ENV RUST_BACKTRACE 1
WORKDIR /usr/src/app
COPY . .
RUN cargo build --release
EXPOSE 8080
CMD ["./target/release/app"]
